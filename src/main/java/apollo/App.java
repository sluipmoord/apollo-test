package apollo;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.linq.LoadDeductionMutation;
import com.apollographql.apollo.linq.PayrollsQuery;
import com.apollographql.apollo.linq.UpdateDeductionMutation;
import com.apollographql.apollo.linq.type.DeductionType;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

public class App {

    private static QlinkClient qlinkClient;

    public App() {

        qlinkClient = new QlinkClient();
    }

    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) {
        App app = new App();
        System.out.println(app.getGreeting());

        String payrollId = "UGF5cm9sbE5vZGU6MDAwMQ==";
        String employeeNumber = "70334030";
        DeductionType deductionType = DeductionType.FUNERAL_INSURANCE;
        String referenceNumber = "1245";
        String surname = "MVUNGE";
        String initials = "KC";
        String idNumber = "8607270439080";
        Integer amount = 160;
        Double arrearsInstalment = null;
        String registrationNumber = "1221";
        Date dateOfBirth = null;

        qlinkClient.payrolls().enqueue(new ApolloCall.Callback<PayrollsQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<PayrollsQuery.Data> response) {
                PayrollsQuery.Payrolls payrolls = response.data().payrolls();
                for (PayrollsQuery.Edge payroll : payrolls.edges()) {
                    String desc = payroll.node().description();
                    String id = payroll.node().id();
                    System.out.println(desc);
                    System.out.println(id);
                }

            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });

        qlinkClient
                .loadDeduction(
                        payrollId,
                        employeeNumber,
                        initials,
                        surname,
                        idNumber,
                        dateOfBirth,
                        referenceNumber,
                        deductionType,
                        amount,
                        registrationNumber,
                        arrearsInstalment
                )
                .enqueue(
                        new ApolloCall.Callback<LoadDeductionMutation.Data>() {

                            @Override
                            public void onResponse(@NotNull Response<LoadDeductionMutation.Data> response) {
                                assert response.data() != null;
                                System.out.println(response.data().loadDeduction().__typename());

                                if (response.data().loadDeduction().__typename().equalsIgnoreCase("SubmissionSuccess")) {
                                    LoadDeductionMutation.AsSubmissionSuccess loadDeduction = (LoadDeductionMutation.AsSubmissionSuccess) response.data().loadDeduction();
                                    LoadDeductionMutation.Submission submission = loadDeduction.submission();
                                    LoadDeductionMutation.Node transaction = submission.transactions().edges().get(0).node();

                                    LoadDeductionMutation.Error error = transaction.error();
                                    System.out.println(loadDeduction.submission());
                                    System.out.println(loadDeduction);
                                    System.out.println(error);
                                } else {
                                    LoadDeductionMutation.AsNotFoundError notFoundError = (LoadDeductionMutation.AsNotFoundError) response.data().loadDeduction();
                                    System.out.println(notFoundError);
                                }
                            }


                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                System.out.println(e.getMessage());
                            }
                        });
    }
}