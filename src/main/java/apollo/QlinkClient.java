package apollo;

import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.ApolloMutationCall;
import com.apollographql.apollo.ApolloQueryCall;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseField;
import com.apollographql.apollo.cache.normalized.CacheKey;
import com.apollographql.apollo.cache.normalized.CacheKeyResolver;
import com.apollographql.apollo.cache.normalized.NormalizedCacheFactory;
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy;
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory;
import com.apollographql.apollo.linq.LoadDeductionMutation;
import com.apollographql.apollo.linq.PayrollsQuery;
import com.apollographql.apollo.linq.type.CustomType;
import com.apollographql.apollo.linq.type.DeductionType;
import com.apollographql.apollo.linq.type.NewDeductionInput;
import com.apollographql.apollo.response.CustomTypeAdapter;
import com.apollographql.apollo.response.CustomTypeValue;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class QlinkClient {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat DATE_TIME_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    protected String qlinkUrl = "https://92fx4xkpz2.execute-api.eu-west-1.amazonaws.com/dev/graphql";

    private ApolloClient apolloClient;

    public QlinkClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.level(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
        NormalizedCacheFactory cacheFactory = new LruNormalizedCacheFactory(EvictionPolicy.builder().maxSizeBytes(10 * 1024).build());

        //Create the cache key resolver, this example works well when all types have globally unique ids.
        CacheKeyResolver resolver = new CacheKeyResolver() {
            @NotNull
            @Override
            public CacheKey fromFieldRecordSet(@NotNull ResponseField field, @NotNull Map<String, Object> recordSet) {
                return formatCacheKey((String) recordSet.get("id"));
            }

            @NotNull
            @Override
            public CacheKey fromFieldArguments(@NotNull ResponseField field, @NotNull Operation.Variables variables) {
                return formatCacheKey((String) field.resolveArgument("id", variables));
            }

            private CacheKey formatCacheKey(String id) {
                if (id == null || id.isEmpty()) {
                    return CacheKey.NO_KEY;
                } else {
                    return CacheKey.from(id);
                }
            }
        };

        CustomTypeAdapter dateCustomTypeAdapter =
                new CustomTypeAdapter<Date>() {
                    @Override
                    public Date decode(CustomTypeValue value) {
                        try {
                            return DATE_FORMAT.parse(value.value.toString());
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public CustomTypeValue encode(Date value) {
                        return new CustomTypeValue.GraphQLString(DATE_FORMAT.format(value));
                    }
                };

        CustomTypeAdapter dateTimeCustomTypeAdapter =
                new CustomTypeAdapter<Date>() {
                    @Override
                    public Date decode(CustomTypeValue value) {
                        try {
                            return DATE_TIME_FORMAT.parse(value.value.toString());
                        } catch (ParseException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public CustomTypeValue encode(Date value) {
                        return new CustomTypeValue.GraphQLString(DATE_TIME_FORMAT.format(value));
                    }
                };

        this.apolloClient =
                ApolloClient.builder()
                        .serverUrl(qlinkUrl)
                        .normalizedCache(cacheFactory, resolver)
                        .okHttpClient(okHttpClient)
                        .addCustomTypeAdapter(CustomType.DATE, dateCustomTypeAdapter)
                        .addCustomTypeAdapter(CustomType.DATETIME, dateTimeCustomTypeAdapter)
                        .build();
    }

    public ApolloClient apolloClient() {
        return apolloClient;
    }

    public ApolloMutationCall<LoadDeductionMutation.Data> loadDeduction(
            @NotNull String payrollId,
            @NotNull String employeeNumber,
            @NotNull String initials,
            @NotNull String surname,
            String idNumber,
            Date dateOfBirth,
            String referenceNumber,
            @NotNull DeductionType deductionType,
            double amount,
            @NotNull String registrationNumber,
            Double arrearsInstalment) {
        NewDeductionInput.Builder builder =
                NewDeductionInput.builder()
                        .payrollId(payrollId)
                        .initials(initials)
                        .surname(surname)
                        .idNumber(idNumber)
                        .referenceNumber(referenceNumber)
                        .deductionType(deductionType)
                        .amount(amount)
                        .registrationNumber(registrationNumber)
                        .arrearsInstalment(arrearsInstalment)
                        .employeeNumber(employeeNumber);

        if (dateOfBirth != null) {
            builder.dateOfBirth(dateOfBirth);
        }

        NewDeductionInput input = builder.build();
        LoadDeductionMutation loadDeductionMutation =
                LoadDeductionMutation.builder().input(input).build();

        return apolloClient().mutate(loadDeductionMutation);
    }

    public ApolloQueryCall<PayrollsQuery.Data> payrolls() {
        PayrollsQuery query = PayrollsQuery.builder().build();

        return apolloClient().query(query);
    }
}

